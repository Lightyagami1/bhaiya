# statement starting with `#` are comments, and are not code, will be ignored

# REFERENCES
# pandas : https://pandas.pydata.org/docs/reference/api/pandas.read_excel.html
# anaconda : https://www.anaconda.com/
#          : https://repo.anaconda.com/archive/Anaconda3-2020.11-Windows-x86_64.exe


# NOTE: all extra library are added here
import pandas as pd
import openpyxl
import sqlite3
import pdb
import csv

"""
pandas, openpyxl are extra dependency needs to be installed explictly by user
or can install anaconda environment for python, which has this pre-installed
"""

base_path = "/Users/saurav/Downloads/bhaiya/"

def read_file_excel(path):
    return pd.ExcelFile(path, engine="openpyxl")

def show_data(data):
    """
    this function shows the content of file read from read_csv function
    """
    print(data.head()) # <- header of file
    for ind,line in data.iterrows(): # <- to print data in file
        print("************************************************************************")
        print(line)

def sales_data():
    sdata = read_file_excel(base_path + "GRG A_B_C wise shop lists + Rack Deployment + standee.xlsx")
    return sdata

def consolidated_file():
    cdata = read_file_excel(base_path + "Consolidated file - Marketing Socities, Shops .xlsx")
    return cdata

def grg_data():
    temp = pd.read_csv(base_path+"Consolidated file - Marketing Socities, Shops - grg rETAILER oNBOARD lIST - mOH.tsv", delimiter="\t")
    temp.columns = temp.iloc[2] # column name are in 3 rd line
    temp = temp[3:] # consider only after 4the line

    temp = temp.sort_values("Name of Shop")
    return temp

def creat_new(old):
    # take out the only required columns
    pruned = old[["Name of Shop", "Sales Person", "CATEGORY", "Cluster","Class","SECTOR","Address","Phone No"]]
    return pruned

def write_to_file(df):
    df.to_csv(r'pandas.txt', header=True, index=None, sep='\t', mode='a')

def main():
    t = grg_data()
    nt = creat_new(t)
    write_to_file(nt)

if __name__ == "__main__": # <- to start code, either "Run from python code application", or python3 code.py from command line interface
    print('hi') # <- print() : screen output : will write `hi` on screen
    main() # <- calling main function to start


